package de.Iclipse.NRaid.API.Main;

import de.Iclipse.NRaid.API.Commands.Completer.com_mode;
import de.Iclipse.NRaid.API.Commands.Completer.com_server;
import de.Iclipse.NRaid.API.Commands.*;
import de.Iclipse.NRaid.API.Functions.EventListener;
import de.Iclipse.NRaid.API.Functions.Events.CommandEvent;
import de.Iclipse.NRaid.API.Functions.Permission;
import de.Iclipse.NRaid.API.Functions.Slack.SlackApi;
import de.Iclipse.NRaid.API.Functions.Slack.SlackChannel;
import de.Iclipse.NRaid.API.Functions.Slack.SlackMessage;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Mode.createModeTable;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.*;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_User.createUserTable;
import static de.Iclipse.NRaid.API.Main.MainData.*;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class Main extends JavaPlugin {
    public static Plugin instance;
    public static SlackApi teamchannel;
    public static SlackApi sysadmin;
    public static boolean reload = false;


    @Override
    public void onLoad() {
        System.out.println(prefix + "Die API wurde geladen und wird nun initialisiert!");
    }

    @Override
    public void onEnable() {
        System.out.println(prefix + "Der Server ist nun mit der API verbunden!");
        instance = this;
        MySQL.connect();
        Permission.connect();
        sysadmin = new SlackApi("https://hooks.slack.com/services/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");
        teamchannel = new SlackApi("https://hooks.slack.com/services/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");
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        loadCommands();
        createServerTable();
        createModeTable();
        //    MySQL.update("CREATE TABLE IF NOT EXISTS nick(uuid VARCHAR(64), nickname VARCHAR(20), autonick BOOLEAN)");
        if (Bukkit.getOnlinePlayers().size() > 0) {
            Bukkit.getOnlinePlayers().forEach(entry -> {
                addPlayer(Bukkit.getServerName(), entry.getUniqueId());
            });
        } else {
            setPlayers(Bukkit.getServerName(), null);
        }
        if (isServerRegistered(Bukkit.getServer().getServerName())) {
            if (getStatus(Bukkit.getServerName()).equalsIgnoreCase("OFFLINE")) {
                sendSlackMessage(SlackChannel.SYSADMIN, "Der Server " + Bukkit.getServerName() + " wird gestartet!");
                changeServerStatus(Bukkit.getServerName(), "ONLINE");
                System.out.println(sysprefix + "Der Status des Servers §e" + Bukkit.getServerName() + "§7 ist nun: §2" + getStatus(Bukkit.getServerName()) + "§7! Er war bereits registriert!");

            }
        } else {
            sendSlackMessage(SlackChannel.SYSADMIN, "Der Server " + Bukkit.getServerName() + " wird gestartet!");
            registerServer(Bukkit.getServerName(), "ONLINE", 20);
            System.out.println(sysprefix + "Der Status des Servers §e" + Bukkit.getServerName() + "§7 ist nun: §2" + getStatus(Bukkit.getServerName()) + "§7!");
        }
        createUserTable();
        loadListener();
        scoreboard = getServer().getScoreboardManager().getNewScoreboard();
        obj = scoreboard.registerNewObjective("lol", "lul");
        admin = scoreboard.registerNewTeam("a");
        admin.setPrefix("§1");
        admin.setDisplayName(admin.getPrefix() + "Administrator");
        dev = scoreboard.registerNewTeam("c");
        dev.setPrefix("§9");
        dev.setDisplayName(dev.getPrefix() + "Developer");
        mod = scoreboard.registerNewTeam("b");
        mod.setPrefix("§4");
        mod.setDisplayName(mod.getPrefix() + "Moderator");
        sup = scoreboard.registerNewTeam("d");
        sup.setPrefix("§c");
        sup.setDisplayName(sup.getPrefix() + "Supporter");
        builder = scoreboard.registerNewTeam("e");
        builder.setPrefix("§2");
        builder.setDisplayName(builder.getPrefix() + "Builder");
        yt = scoreboard.registerNewTeam("f");
        yt.setPrefix("§5");
        yt.setDisplayName(yt.getPrefix() + "YouTuber");
        premium = scoreboard.registerNewTeam("g");
        premium.setPrefix("§6");
        premium.setDisplayName(premium.getPrefix() + "Premium");
        user = scoreboard.registerNewTeam("h");
        user.setPrefix("§7");
        user.setDisplayName(user.getPrefix() + "User");
        try {
            File file = new File("icon.jpg");
            if (!file.exists()) {
                BufferedImage img = ImageIO.read(new URL("https://yt3.ggpht.com/-KzF2NfEDkjY/AAAAAAAAAAI/AAAAAAAAAAA/22HOL_q0SW4/s288-c-k-no-mo-rj-c0xffffff/photo.jpg"));
                ImageIO.write(img, "jpg", file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDisable() {
        if (!reload) {
            changeServerStatus(Bukkit.getServerName(), "OFFLINE");
            sendSlackMessage(SlackChannel.SYSADMIN, "Der Server " + Bukkit.getServerName() + " wird gestoppt!");
        }
        setPlayers(Bukkit.getServerName(), new ArrayList<>());
        MySQL.close();
        Permission.close();
    }

    public void loadListener() {
        Bukkit.getServer().getPluginManager().registerEvents(new EventListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new CommandEvent(), this);
    }

    public void loadCommands() {
        getCommand("gamemode").setExecutor(new cmd_Gamemode());
        getCommand("color").setExecutor(new cmd_colour());
        getCommand("vanish").setExecutor(new cmd_Vanish());
        getCommand("vv").setExecutor(new cmd_Vanish());
        getCommand("points").setExecutor(new cmd_Points());
        getCommand("setpoints").setExecutor(new cmd_Points());
        getCommand("addpoints").setExecutor(new cmd_Points());
        getCommand("removepoints").setExecutor(new cmd_Points());
        getCommand("mode").setExecutor(new cmd_modus());
        getCommand("chatclear").setExecutor(new cmd_chatclear());
        getCommand("servers").setExecutor(new cmd_server());
        getCommand("api").setExecutor(new cmd_api());
        getCommand("mode").setTabCompleter(new com_mode());
        getCommand("servers").setTabCompleter(new com_server());
    }

    public static void sendJSONMassage(Player p, String jsonmessage) {
        IChatBaseComponent chat = new IChatBaseComponent.ChatSerializer().a(jsonmessage);
        PacketPlayOutChat packet = new PacketPlayOutChat(chat);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendSlackMessage(SlackChannel channel, String message) {
        channel.getApi().call((new SlackMessage(message)).setIcon(":loudspeaker:").setUsername(channel.getName()));
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        return bimage;
    }

}
