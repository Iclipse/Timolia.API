package de.Iclipse.NRaid.API.Functions.Events;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.*;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 02.12.2017 at 18:13 o´ clock
 */
public class CommandEvent extends Event implements Listener, Cancellable {
    CommandSender sender;
    String cmd;
    String[] args;
    private boolean cancel = false;

    public CommandEvent() {
    }

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public CommandEvent(CommandSender sender, String cmd, String[] args) {
        this.sender = sender;
        this.cmd = cmd;
        this.args = args;
    }

    public String[] getArgs() {
        return args;
    }

    public String getCommand() {
        return cmd;
    }

    public CommandSender getSender() {
        return sender;
    }

    public boolean isCancelled() {
        return this.cancel;
    }

    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    @EventHandler
    public void on(ServerCommandEvent e) {
        CommandSender sender = e.getSender();
        String cmd = e.getCommand().split(" ")[0].replaceFirst("/", "");
        String[] args = e.getCommand().replaceFirst("/" + cmd + " ", "").split(" ");
        Bukkit.getPluginManager().callEvent(new CommandEvent(sender, cmd, args));
    }

    @EventHandler
    public void on(PlayerCommandPreprocessEvent e) {
        CommandSender sender = e.getPlayer();
        String cmd = e.getMessage().split(" ")[0].replaceFirst("/", "");
        String[] args = e.getMessage().replaceFirst("/" + cmd + " ", "").split(" ");
        Bukkit.getPluginManager().callEvent(new CommandEvent(sender, cmd, args));
    }
}
