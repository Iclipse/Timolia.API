package de.Iclipse.NRaid.API.Functions.Slack;

import de.Iclipse.NRaid.API.Main.Main;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 18.11.2017 at 12:10 o´ clock
 */
public enum SlackChannel {
    SYSADMIN(Main.sysadmin, "Root"),
    TEAMCHANNEL(Main.teamchannel, "NRaidTeam");

    private final SlackApi api;
    private final String name;

    private SlackChannel(SlackApi api, String name) {
        this.api = api;
        this.name = name;
    }

    public SlackApi getApi() {
        return this.api;
    }

    public String getName() {
        return this.name;
    }
}
