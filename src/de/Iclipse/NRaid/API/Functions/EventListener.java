package de.Iclipse.NRaid.API.Functions;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.ServerListPingEvent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.*;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_User.isUserOnline;
import static de.Iclipse.NRaid.API.Functions.RankColour.setTeam;
import static de.Iclipse.NRaid.API.Main.Main.instance;
import static de.Iclipse.NRaid.API.Main.MainData.scoreboard;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class EventListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        Tablist.setTablist(e.getPlayer().getUniqueId());
        setNames(p);
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        System.out.println(e.getReason());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        Bukkit.getScheduler().runTaskLater(instance, new Runnable() {
            @Override
            public void run() {
                if (!isUserOnline(p.getUniqueId())) {
                    p.kickPlayer("§cDu darfst (natürlich nicht) über einen anderen Proxy joinen!!!");
                }
            }
        }, 20);
        p.setScoreboard(scoreboard);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (getPlayers(Bukkit.getServerName()).contains(e.getPlayer().getUniqueId())) {
            removePlayer(Bukkit.getServerName(), e.getPlayer());
        }
        if (getSpecs(Bukkit.getServerName()).contains(e.getPlayer().getUniqueId())) {
            removeSpec(Bukkit.getServerName(), e.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        e.setMotd("§3NRaid.de §7- §6Dein CWBW Trainings Server!\n §c§lWIRD GERADE GEWARTET!");
        e.setMaxPlayers(9999999);
        try {
            BufferedImage img = ImageIO.read(new URL("https://yt3.ggpht.com/-KzF2NfEDkjY/AAAAAAAAAAI/AAAAAAAAAAA/22HOL_q0SW4/s288-c-k-no-mo-rj-c0xffffff/photo.jpg"));
            e.setServerIcon(Bukkit.loadServerIcon(ImageIO.read(new File("icon.jpg"))));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @EventHandler
    public void onDisable(PluginDisableEvent e) {
        changeServerStatus(Bukkit.getServerName(), "OFFLINE");
    }

    @EventHandler
    public void onPlayerChat(PlayerChatEvent e) {
        Player p = e.getPlayer();
        String message = e.getMessage();
        if (p.hasPermission("nraid.builder.chat")) {
            if (!isTeamMessage(message)) {
                message = ChatColor.translateAlternateColorCodes('$', message);
            }
        }
        e.setFormat(p.getDisplayName() + "§7: " + message);
    }

    public static void setNames(Player p) {
        setTeam(p);
        p.setDisplayName(RankColour.getRankColour(p) + p.getName());
        p.setCustomName(RankColour.getRankColour(p) + p.getName());
        p.setCustomNameVisible(true);
        addPlayer(Bukkit.getServerName(), p);
    }


    public static boolean isTeamMessage(String message) {
        if (message.startsWith("@team") || message.startsWith("@t") || message.startsWith("@Team")) {
            return true;
        }
        return false;
    }



}
