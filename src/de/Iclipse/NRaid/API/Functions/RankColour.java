package de.Iclipse.NRaid.API.Functions;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import static de.Iclipse.NRaid.API.Main.MainData.*;

/**
 * Created by Iclipse and a bit of Kenai who don't like people which steal this code!!
 */
public class RankColour {

    public static String getRankColour(Player p) {
        return getTeam(p).getPrefix();
    }

    public static void setTeam(Player p) {
        Team t;
            if (p.hasPermission("nraid.color.administrator")) {
                t = admin;
            } else if (p.hasPermission("nraid.color.developer")) {
                t = dev;
            } else if (p.hasPermission("nraid.color.moderator")) {
                t = mod;
            } else if (p.hasPermission("nraid.color.supporter")) {
                t = sup;
            } else if (p.hasPermission("nraid.color.builder")) {
                t = builder;
            } else if (p.hasPermission("nraid.color.youtuber")) {
                t = yt;
            } else if (p.hasPermission("nraid.color.premium")) {
                t = premium;
            } else {
                t = user;
            }
        if (!t.hasPlayer(p)) {
            t.addPlayer(p);
            System.out.println("Player wird dem Team" + t.getDisplayName() + " hinzugefügt!");
        }

    }

    public static Team getTeam(Player p) {
        if (p.hasPermission("nraid.color.administrator")) {
            return admin;
        } else if (p.hasPermission("nraid.color.developer")) {
            return dev;
        } else if (p.hasPermission("nraid.color.moderator")) {
            return mod;
        } else if (p.hasPermission("nraid.color.supporter")) {
            return sup;
        } else if (p.hasPermission("nraid.color.builder")) {
            return builder;
        } else if (p.hasPermission("nraid.color.youtuber")) {
            return yt;
        } else if (p.hasPermission("nraid.color.premium")) {
            return premium;
        } else {
            return user;
        }
    }
}
