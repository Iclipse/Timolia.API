package de.Iclipse.NRaid.API.Functions;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.UUID;

/**
 * Created by Iclipse and a bit of Kenai who don't like people which steal this code!!
 */
public class Tablist{


    static String header = "§8§l« §3§lNight§6§lRaid§8§l »";
    static String footer = "§7Server: §e" + Bukkit.getServerName();
    static String port = "§7Port: §e" + Bukkit.getPort();
    static String ranks = "§1Admin §9Dev §4Mod §cSup §2Builder §5YouTuber§4§k§r §6Premium";

    public Tablist() {
        super();
    }

    static Field bfield;

    public static void setTablist(UUID uuid) {
        Player p = Bukkit.getPlayer(uuid);
        CraftPlayer cp = (CraftPlayer) p;

        IChatBaseComponent tabfooter;
        if (Bukkit.getPlayer(uuid).hasPermission("nraid.moderator.serversettings")) {
            tabfooter = IChatBaseComponent.ChatSerializer.a("{\"text\" : \"" + ranks + "\n" + footer + "\n" + port + "\"}");
        } else tabfooter = IChatBaseComponent.ChatSerializer.a("{\"text\" : \"" + ranks + "\n" + footer + "\"}");
        IChatBaseComponent tabheader;
        tabheader = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter(tabheader);
        try {
            if (bfield == null) {
                bfield = packet.getClass().getDeclaredField("b");
                bfield.setAccessible(true);
            }
            bfield.set(packet, tabfooter);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        cp.getHandle().playerConnection.sendPacket(packet);
    }
}
