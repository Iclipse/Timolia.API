package de.Iclipse.NRaid.API.Functions.BanSystem;

import de.Iclipse.NRaid.API.Main.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_User.getIP;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 20.12.2017 at 14:33 o´ clock
 */
public class MySQL_Ban {
    public static void createBanTable() {
        MySQL.update("CREATE TABLE IF NOT EXISTS bans(banid INT(6) NOT NULL AUTO_INCREMENT, uuid VARCHAR(20), ip VARCHAR(20), date DATETIME, reason VARCHAR(40), strength INT(2), executor VARCHAR(20), end BIGINT(10), endreason VARCHAR(50), right BOOLEAN)");
    }

    public static void createBan(UUID uuid, String reason, int strength, String executor) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time = Date.from(Instant.now());
        long millis;
        if (getCurrentBanStrength(uuid) + strength < 5) {
            millis = System.currentTimeMillis() + (strength * 30);
        } else {
            millis = Instant.MAX.toEpochMilli();
        }
        MySQL.update("INSERT INTO `bans` (uuid, ip, date, reason, strength, executor, end, endreason, right) VALUES ('" + uuid + "', '" + getIP(uuid) + "', '" + sdf.format(time) + "', '" + reason + "', " + strength + ",'" + executor + "', " + millis + ", 'RUNNING', 'true'");
    }

    public static boolean isBanned(UUID uuid) {
        ResultSet rs = MySQL.querry("SELECT ended FROM `bans` WHERE uuid = '" + uuid + "' AND ended = 'RUNNING'");
        try {
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static long getBanLength(UUID uuid) {
        ResultSet rs = MySQL.querry("SELECT end FROM `bans` WHERE uuid = '" + uuid + "' AND ended = 'RUNNING'");
        try {
            while (rs.next()) {
                return rs.getLong("end") - System.currentTimeMillis();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public static int getCurrentBanStrength(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT strength FROM `bans` WHERE uuid = '" + uuid + "' AND right = 'true'");
            int i = 0;
            while (rs.next()) {
                i += rs.getInt("strength");
            }
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static boolean isPermaBanned(UUID uuid) {
        ResultSet rs = MySQL.querry("SELECT end FROM `bans` WHERE uuid = '" + uuid + "' AND right = 'true'");
        try {
            long i = 0;
            while (rs.next()) {
                i = rs.getLong("end");
            }
            if (i == Instant.MAX.toEpochMilli()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
