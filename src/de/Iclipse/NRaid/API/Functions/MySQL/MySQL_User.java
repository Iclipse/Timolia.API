package de.Iclipse.NRaid.API.Functions.MySQL;

import de.Iclipse.NRaid.API.Main.MySQL;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import static de.Iclipse.NRaid.API.Functions.UUIDFetcher.getUUID;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class MySQL_User {
    public static void createUserTable() {
        MySQL.update("CREATE TABLE IF NOT EXISTS user(uuid VARCHAR(64), ip VARCHAR(20), status VARCHAR(20), points INT(64), onlinetime BIGINT(20), firsttime DATETIME, lastseen BIGINT(20))");
    }

    public static void registerUser(Player p) {
        UUID uuid = getUUID(p.getName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time = Date.from(Instant.now());
        MySQL.update("INSERT INTO `user` VALUES ('" + uuid.toString() + "', " + p.getAddress().toString().replace(":" + p.getAddress().getPort(), "") + ",'ONLINE', 0, 0, '" + sdf.format(time) + "', -1)");
    }

    public static boolean isUserExists(Player p) {
        try {
            ResultSet rs = MySQL.querry("SELECT status FROM `user` WHERE uuid = '" + p.getUniqueId() + "'");
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    public static boolean isUserExists(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT status FROM `user` WHERE uuid = '" + uuid + "'");
            return rs.next();
        } catch (SQLException e) {
            return false;
        }
    }

    public static ArrayList<UUID> getUsers(){
        ArrayList<UUID> list = new ArrayList<>();
        try{
            ResultSet rs = MySQL.querry("SELECT uuid FROM `user` WHERE 1");
            UUID uuid;
            while(rs.next()){
                list.add(UUID.fromString(rs.getString("uuid")));
            }
            return list;
        }catch(SQLException e){
            return null;
        }
    }



    public static boolean isUserOnline(UUID uuid){
        if(getStatus(uuid).equalsIgnoreCase("OFFLINE")){
            return false;
        }
        return true;
    }


    public static String getStatus(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT status FROM `user` WHERE uuid = '" + uuid + "'");
            while(rs.next()){
                return rs.getString("status");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStatus(Player p) {
        try {
            ResultSet rs = MySQL.querry("SELECT status FROM `user` WHERE uuid = '" + p.getUniqueId() + "'");
            while (rs.next()) {
                return rs.getString("status");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void switchStatus(UUID uuid, String s) {
        MySQL.update("UPDATE `user` SET status = '" + s + "' WHERE uuid = '" + uuid.toString() + "'");
    }

    public static void setPoints(UUID uuid, int points){
        MySQL.update("UPDATE `user` SET points = " + points + " WHERE uuid = '" + uuid.toString() + "'");
    }

    public static Integer getPoints(UUID uuid) {
        ResultSet rs = MySQL.querry("SELECT points FROM `user` WHERE uuid = '" + uuid.toString() + "'");
        try {
            while(rs.next()){
                return rs.getInt("points");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void addPoints(UUID uuid, int points){
        setPoints(uuid, getPoints(uuid) + points);
    }
    public static void removePoints(UUID uuid, int points){
        setPoints(uuid, getPoints(uuid) - points);
    }


    public static long getOnlineTime(Player pp) {
        try {
            ResultSet rs = MySQL.querry("SELECT onlinetime FROM `user` WHERE uuid = '" + pp.getUniqueId() + "'");
            while (rs.next()) {
                long time = rs.getLong("onlinetime");
                System.out.println("getOnlineTime: " + time);
                return time;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public static long getOnlineTime2(Player pp) {
        try {
            ResultSet rs = MySQL.querry("SELECT onlinetime FROM `user` WHERE uuid = '" + pp.getUniqueId() + "'");
            while (rs.next()) {
                long time = rs.getLong("onlinetime");
                System.out.println("getOnlineTime2: " + time);
                return time;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static long getOnlineTime2(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT onlinetime FROM `user` WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                long time = rs.getLong("onlinetime");
                System.out.println("getOnlineTime2: " + time);
                return time;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void setOnlineTime(Player pp, long time) {
        MySQL.update("UPDATE `user` SET onlinetime = " + time + " WHERE uuid = '" + pp.getUniqueId() + "'");
    }

    public static void setOnlineTime(UUID uuid, long time) {
        MySQL.update("UPDATE `user` SET onlinetime = " + time + " WHERE uuid = '" + uuid + "'");
    }

    public static void addOnlineTime(UUID uuid, long time) {
        setOnlineTime(uuid, getOnlineTime2(uuid) + time);
    }

    public static void addOnlineTime(Player pp, long time) {
        setOnlineTime(pp, getOnlineTime2(pp) + time);
    }


    public static LocalDateTime getFirstTime(UUID uuid) {
        ResultSet rs = MySQL.querry("SELECT  firsttime FROM `user` WHERE uuid = '" + uuid.toString() + "'");
        try {
            while (rs.next()) {
                return LocalDateTime.of(rs.getDate("firsttime").toLocalDate(), rs.getTime("firsttime").toLocalTime());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static LocalDateTime getFirstTime(Player pp) {
        ResultSet rs = MySQL.querry("SELECT  firsttime FROM `user` WHERE uuid = '" + pp.getUniqueId().toString() + "'");
        try {
            while (rs.next()) {
                return LocalDateTime.of(rs.getDate("firsttime").toLocalDate(), rs.getTime("firsttime").toLocalTime());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getLastTime(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT lastseen FROM `user` WHERE uuid = '" + uuid + "'");
            return rs.getLong("lastseen");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static long getLastTime(Player pp) {
        UUID uuid = pp.getUniqueId();
        try {
            ResultSet rs = MySQL.querry("SELECT lastseen FROM `user` WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getLong("lastseen");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void setLastTime(UUID uuid, long time) {
        MySQL.update("UPDATE `user` SET lastseen = " + time + " WHERE uuid = '" + uuid + "'");
    }

    public static void setLastTime(Player pp, long time) {
        UUID uuid = pp.getUniqueId();
        MySQL.update("UPDATE `user` SET lastseen = " + time + " WHERE uuid = '" + uuid + "'");
    }

    public static void setIP(Player p) {
        MySQL.update("UPDATE `user` SET ip = '" + p.getAddress().toString().replace(":" + p.getAddress().getPort(), "").replaceFirst("/", "") + "' WHERE uuid = '" + p.getUniqueId() + "'");
    }

    public static String getIP(UUID uuid) {
        try {
            ResultSet rs = MySQL.querry("SELECT ip FROM `user` WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getString("ip");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
