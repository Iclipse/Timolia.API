package de.Iclipse.NRaid.API.Functions.MySQL;

import de.Iclipse.NRaid.API.Functions.GameSign;
import de.Iclipse.NRaid.API.Main.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 24.10.2017 at 16:26 o´ clock
 */
public class MySQL_Signs {

    public static void createSignTable() {
        MySQL.update("CREATE TABLE IF NOT EXISTS signs(id INT(3), server VARCHAR(20), xk DOUBLE, yk DOUBLE, zk DOUBLE, modus VARCHAR(20), curserver VARCHAR(20))");
    }

    public static void createSign(Location loc, String modename) {
        MySQL.update("INSERT INTO `signs` VALUES (" + getLowestId() + ", '" + Bukkit.getServerName() + "', " + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + ", '" + modename + "', 'NONE')");
    }

    public static void removeSign(Location loc) {
        MySQL.update("DELETE FROM `signs` WHERE server = '" + Bukkit.getServerName() + "' AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ() + " AND xk = " + loc.getBlockX());
        updateSignIds();
    }

    public static void removeSign(String server, Location loc) {
        MySQL.update("DELETE FROM `signs` WHERE server = '" + server
                + "' AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ() + " AND xk = " + loc.getBlockX());
        updateSignIds();
    }

    public static void removeSign(int id) {
        MySQL.update("DELETE FROM `signs` WHERE id = " + id);
        updateSignIds();
    }

    public static void removeSign(String curserver) {
        MySQL.update("DELETE FROM `signs` WHERE curserver = '" + curserver + "' AND server = '" + Bukkit.getServerName() + "'");
        updateSignIds();
    }

    public static void removeSign(String server, String curserver) {
        MySQL.update("DELETE FROM `signs` WHERE curserver = '" + curserver + "' AND server = '" + server + "'");
        updateSignIds();
    }

    public static void updateSignIds() {
        for (int id = getLowestId() + 1; id < get2LowestId(); id++) {
            getSign(id).setId(id--);
        }
    }


    public static int getLowestId() {
        int i = 0;
        while (isSignExists(i)) {
            i++;
        }
        return i;
    }

    public static int get2LowestId() {
        int i = 0;
        while (isSignExists(i)) {
            i++;
        }
        while (isSignExists(i)) {
            i++;
        }
        return i;
    }

    public static void setId(String server, String curserver, int id) {
        MySQL.update("UPDATE `signs` SET id = " + id + " WHERE server = '" + server + "' AND curserver = '" + curserver + "'");
    }

    public static int getId(String server, String curserver) {
        try {
            ResultSet rs = MySQL.querry("SELECT id FROM `signs` WHERE server = '" + server + "' AND curserver = '" + curserver + "'");
            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int getId(GameSign gs) {
        String server = gs.getServer();
        String curserver = gs.getCurserver();
        try {
            ResultSet rs = MySQL.querry("SELECT id FROM `signs` WHERE server = '" + server + "' AND curserver = '" + curserver + "'");
            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int getId(Location loc) {
        try {
            ResultSet rs = MySQL.querry("SELECT id FROM `signs` WHERE xk = " + loc.getBlockX() + " AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ());
            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static void changeSignServer(int id, String servername) {
        MySQL.update("UPDATE `signs` SET curserver = '" + servername + "' WHERE id = " + id);
    }


    public static String getServer(int id) {
        try {
            ResultSet rs = MySQL.querry("SELECT server FROM `signs` WHERE id = " + id);
            while (rs.next()) {
                return rs.getString("server");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getCurServer(Location loc) {
        try {
            ResultSet rs = MySQL.querry("SELECT curserver FROM `signs` WHERE xk = " + loc.getBlockX() + " AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ());
            while (rs.next()) {
                return rs.getString("curserver");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurServer(int id) {
        try {
            ResultSet rs = MySQL.querry("SELECT curserver FROM `signs` WHERE id = " + id);
            while (rs.next()) {
                return rs.getString("curserver");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Location getLocation(String server, String curserver) {
        try {
            ResultSet rs = MySQL.querry("SELECT xk, yk, zk FROM `signs` WHERE curserver = '" + curserver + "' AND server = '" + server + "'");
            while (rs.next()) {
                return new Location(Bukkit.getWorld("world"), rs.getDouble("xk"), rs.getDouble("yk"), rs.getDouble("zk"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Location getLocation(int id) {
        try {
            ResultSet rs = MySQL.querry("SELECT xk, yk, zk FROM `signs` WHERE id = " + id);
            while (rs.next()) {
                return new Location(Bukkit.getWorld("world"), rs.getDouble("xk"), rs.getDouble("yk"), rs.getDouble("zk"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isSignExists(int id) {
        try {
            ResultSet rs = MySQL.querry("SELECT modus FROM `signs` WHERE id = " + id);
            return rs.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isSignExists(Location loc) {
        try {
            ResultSet rs = MySQL.querry("SELECT curserver FROM `signs` WHERE xk = " + loc.getBlockX() + " AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ());
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isSignExists(String curserver) {
        final boolean[] b = {false};
        try {
            getSigns().forEach(entry -> {
                if (entry.getCurserver().equals(curserver)) {
                    b[0] = true;
                } else {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b[0];
    }

    public static String getMode(Location loc) {
        try {
            ResultSet rs = MySQL.querry("SELECT modus FROM `signs` WHERE xk = " + loc.getBlockX() + " AND yk = " + loc.getBlockY() + " AND zk = " + loc.getBlockZ());
            while (rs.next()) {
                return rs.getString("modus");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMode(int id) {
        try {
            ResultSet rs = MySQL.querry("SELECT modus FROM `signs` WHERE id = " + id);
            while (rs.next()) {
                return rs.getString("modus");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<GameSign> getSigns(String server) {
        ArrayList<GameSign> list = new ArrayList<>();
        for (int i = 0; i < get2LowestId(); i++) {
            if (isSignExists(i) && getServer(i).equalsIgnoreCase(server)) {
                list.add(getSign(i));
            }
        }
        return list;
    }

    public static ArrayList<GameSign> getSigns() {
        ArrayList<GameSign> list = new ArrayList<>();
        for (int i = 0; i < get2LowestId(); i++) {
            if (isSignExists(i) && getServer(i).equalsIgnoreCase(Bukkit.getServerName())) {
                list.add(getSign(i));
            }
        }
        return list;
    }

    public static ArrayList<GameSign> getESigns(String mode) {
        ArrayList<GameSign> list = new ArrayList<>();
        for (int i = 0; i < getLowestId(); i++) {
            if (isSignExists(i) && getServer(i).equalsIgnoreCase(Bukkit.getServerName()) && getMode(i).equalsIgnoreCase(mode)) {
                list.add(getSign(i));
            }
        }
        return list;
    }

    public static ArrayList<GameSign> getWOCSigns() {
        ArrayList<GameSign> list = new ArrayList<>();
        for (int i = 0; i < getLowestId(); i++) {
            if (isSignExists(i) && getServer(i).equalsIgnoreCase(Bukkit.getServerName()) && getCurServer(i).equalsIgnoreCase("NONE")) {
                list.add(getSign(i));
            }
        }
        return list;
    }

    public static ArrayList<GameSign> getAllSigns() {
        ArrayList<GameSign> list = new ArrayList<>();
        for (int i = 0; i < getLowestId(); i++) {
            if (isSignExists(i)) {
                list.add(getSign(i));
            }
        }
        return list;
    }

    public static GameSign getSign(int id) {
        return new GameSign(id, getServer(id), getMode(id), getCurServer(id), getLocation(id));
    }

    public static GameSign getSign(String server, Location loc) {
        int id = getId(loc);
        return new GameSign(id, server, getMode(id), getCurServer(id), loc);
    }

    public static GameSign getSign(Location loc) {
        int id = getId(loc);
        return new GameSign(id, Bukkit.getServerName(), getMode(id), getCurServer(id), loc);
    }
}
