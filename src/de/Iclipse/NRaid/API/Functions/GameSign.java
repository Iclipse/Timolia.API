package de.Iclipse.NRaid.API.Functions;

import de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Signs;
import org.bukkit.Location;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Signs.*;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 01.11.2017 at 18:35 o´ clock
 */
public class GameSign {
    //  private static ArrayList<GameSign> list;
    private int id;
    private String server;
    private String mode;
    private String curserver;
    private Location loc;

    public GameSign(int id, String server, String mode, String curserver, Location loc) {
        this.id = id;
        this.server = server;
        this.mode = mode;
        this.curserver = curserver;
        this.loc = loc;
        // list.add(this);
    }

    /*
    public static ArrayList<GameSign> getSigns(){
        setSigns();
        return list;
    }

    public static void setSigns(){
        GameSign.list = getSigns();
    }
*/

    public static GameSign getSign(String curserver) {
        for (GameSign entry : getSigns()) {
            if (entry.getCurserver().equalsIgnoreCase(curserver)) {
                return entry;
            }
        }
        return null;
    }

    public String getServer() {
        return server;
    }


    public String getMode() {
        return mode;
    }


    public String getCurserver() {
        curserver = getCurServer(id);
        return curserver;
    }

    public void setCurserver(String curserver) {
        changeSignServer(id, curserver);
        this.curserver = curserver;
    }

    public Location getLoc() {
        return loc;
    }

    public String toString() {
        return "Sign" + id + ": Server = " + server + ", Modus = " + getMode() + ", CurServer = " + getCurserver() + ", XK = " + loc.getBlockX() + ", YK = " + loc.getBlockY() + ", ZK = " + loc.getBlockZ();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        MySQL_Signs.setId(server, curserver, id);
        this.id = id;
    }

}
