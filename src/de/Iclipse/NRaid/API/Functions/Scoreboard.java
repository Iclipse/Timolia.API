package de.Iclipse.NRaid.API.Functions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.util.HashMap;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class Scoreboard {

        DisplaySlot ds;
        String name;
        Objective obj;
        org.bukkit.scoreboard.Scoreboard board;
        HashMap<Integer, String> lines = new HashMap();

        public Scoreboard()
        {
            org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective obj = board.registerNewObjective("l", "o");
            this.board = board;
            this.obj = obj;
        }

        public Scoreboard(String name)
        {
            org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective obj = board.registerNewObjective("l", "o");
            obj.setDisplayName(name);
            this.name = name;
            this.board = board;
            this.obj = obj;
        }

        public Scoreboard(DisplaySlot ds)
        {
            org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective obj = board.registerNewObjective("l", "o");
            obj.setDisplaySlot(ds);
            this.ds = ds;
            this.board = board;
            this.obj = obj;
        }

        public Scoreboard(DisplaySlot ds, String name)
        {
            org.bukkit.scoreboard.Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective obj = board.registerNewObjective("l", "o");
            obj.setDisplaySlot(ds);
            obj.setDisplayName(name);
            this.ds = ds;
            this.name = name;
            this.board = board;
            this.obj = obj;
        }

        public String getName()
        {
            return this.name;
        }

        public void setName(String name)
        {
            this.obj.setDisplayName(name);
            this.name = name;
        }

        public DisplaySlot getDs()
        {
            return this.ds;
        }

        public void setDs(DisplaySlot ds)
        {
            this.obj.setDisplaySlot(ds);
            this.ds = ds;
        }

        public void setLine(String s, int line)
        {
            this.obj.getScore(s).setScore(line);
            this.lines.put(Integer.valueOf(line), s);
        }

        public String getLine(int line)
        {
            if (this.lines.containsKey(Integer.valueOf(line))) {
                return (String)this.lines.get(Integer.valueOf(line));
            }
            return null;
        }
        public void setScoreboard(Player p){
            p.setScoreboard(board);
        }
}


