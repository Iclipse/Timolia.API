package de.Iclipse.NRaid.API.Functions;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.entity.Player;

import static de.Iclipse.NRaid.API.Main.Main.instance;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 29.10.2017 at 17:06 o´ clock
 */
public class Server {
    public static void sendServer(Player p, String server){
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(instance, "BungeeCord", out.toByteArray());
    }
}
