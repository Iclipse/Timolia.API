package de.Iclipse.NRaid.API.Functions;


import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.sql.*;
import java.util.UUID;

import static de.Iclipse.NRaid.API.Functions.UUIDFetcher.getName;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 01.11.2017 at 20:53 o´ clock
 */
public class Permission {

    public static File getBPFile(){
        return new File("plugins/BungeePerms", "config.yml");
    }

    public static FileConfiguration getBPFileConfiguration(){
        return YamlConfiguration.loadConfiguration(getBPFile());
    }


    public static void readBPFile(){
        FileConfiguration cfg = getBPFileConfiguration();
        HOST = cfg.getString("bungeeperms.general.mysqlhost");
        DATABASE = cfg.getString("bungeeperms.general.mysqldb");
        USER = cfg.getString("bungeeperms.general.mysqluser");
        PASSWORD = cfg.getString("bungeeperms.general.mysqlpw");
        prefix = "§5NRaidBP §3MySQL §8§7";
    }


    private static String HOST;
    private static String DATABASE;
    private static String USER;
    private static String PASSWORD;
    private static String prefix;

    public static Connection conn;
    public Permission(String database){
        this.DATABASE = database;
        connect();
    }

    public static void connect() {
        readBPFile();
        try{
            conn = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + DATABASE + "?autoReconnect=false", USER, PASSWORD);
            System.out.println(prefix + "Verbunden!");
        }catch(SQLException e){
            System.out.println(prefix + "Keine Verbindung! Fehler: " + e.getMessage());
        }
    }
    public static void close(){
        try {
            if(conn != null){
                conn.close();
                System.out.println(prefix + "erfolgreich getrennt!");
            }

        } catch (SQLException e) {
            System.out.println(prefix + "Keine Verbindung! Fehler: " + e.getMessage());
        }
    }

    public static ResultSet querry(String querry){
        ResultSet rs = null;

        Statement st;
        try {
            st = conn.createStatement();
            rs = st.executeQuery(querry);
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
        return rs;
    }

    public static String getRank(UUID uuid){
        String name = getName(uuid);
        System.out.println(name);
        try{
            ResultSet rs = querry("SELECT value FROM `bungeeperms_permissions2` WHERE `key` = 'groups' AND `name` = '" + name + "'");
            while(rs.next()){
                return rs.getString("value");
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }


}
