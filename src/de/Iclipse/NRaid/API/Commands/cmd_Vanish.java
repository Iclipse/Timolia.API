package de.Iclipse.NRaid.API.Commands;

import de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Vanish;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static de.Iclipse.NRaid.API.Main.MainData.gamememodeprefix;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class cmd_Vanish implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(args.length == 0) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if(MySQL_Vanish.isVanish(p)){
                    MySQL_Vanish.setNormal(p);
                    p.sendMessage(gamememodeprefix + "Du bist nun wieder sichtbar, bleibst aber in deinem Gamemode!!!");
                    Bukkit.getOnlinePlayers().forEach(pp -> {
                        pp.showPlayer(p);
                    });
                }else{
                    p.sendMessage(gamememodeprefix + "Du bist nun unsichtbar, bleibst aber in deinem Gamemode!");
                    MySQL_Vanish.setVanish(p);
                    Bukkit.getOnlinePlayers().forEach(pp -> {
                        pp.hidePlayer(p);
                    });
                }
            }else{
                System.out.println(gamememodeprefix + "Du musst einen Spieler angeben!");
            }
        }else{

        }
        return true;
    }
    public static ArrayList<String> getAliases(){
        ArrayList<String> aliases = new ArrayList<>();
        aliases.add("vv");
        aliases.add("v");
        return aliases;
    }
}
