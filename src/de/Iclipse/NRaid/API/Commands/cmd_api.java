package de.Iclipse.NRaid.API.Commands;

import de.Iclipse.NRaid.API.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import static de.Iclipse.NRaid.API.Main.Main.instance;
import static de.Iclipse.NRaid.API.Main.MainData.noperm;
import static de.Iclipse.NRaid.API.Main.MainData.sysprefix;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 26.11.2017 at 20:21 o´ clock
 */
public class cmd_api implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                if (sender.hasPermission("bukkit.command.reload")) {
                    if (args.length == 1) {
                        sender.sendMessage(sysprefix + "Der Server wird reloaded!");
                        System.out.println(sysprefix + "Der Server wird reloaded!");
                        Main.reload = true;
                        Bukkit.getScheduler().runTaskLater(instance, new Runnable() {
                            @Override
                            public void run() {
                                Bukkit.reload();
                            }
                        }, 20);
                    } else {
                        String rloadedpls = "";
                        String nrloadedpls = "";
                        for (String arg : args) {
                            boolean reload = false;
                            for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
                                if (plugin.getName().equalsIgnoreCase(arg)) {
                                    if (plugin.getName() == instance.getDescription().getName()) {
                                        Bukkit.getPluginManager().disablePlugin(plugin);
                                        Bukkit.getPluginManager().enablePlugin(plugin);
                                        rloadedpls = rloadedpls + "§5" + plugin.getName() + "§7, ";
                                        Main.reload = true;
                                        reload = true;
                                    }
                                }
                            }
                            if (reload = false) {
                                nrloadedpls = rloadedpls + "§5" + arg + "§7, ";
                            }
                        }
                        if (rloadedpls != null) {
                            if (rloadedpls.split(", ").length == 1) {
                                sender.sendMessage(sysprefix + "Das Plugin " + rloadedpls + " wurde §2erfolgreich§7 reloaded!");
                            } else {
                                sender.sendMessage(sysprefix + "Die Plugins " + rloadedpls + " wurden §2erfolgreich§7 reloaded!");
                            }
                        }
                        if (nrloadedpls != null) {
                            if (nrloadedpls.split(", ").length == 1) {
                                sender.sendMessage(sysprefix + "Das Plugin " + nrloadedpls + " konnte §4NICHT§7 reloaded werden!");
                            } else {
                                sender.sendMessage(sysprefix + "Die Plugins " + nrloadedpls + " konnten §4NICHT§7 reloaded werden!");
                            }
                        }
                    }
                } else {
                    sender.sendMessage(noperm);
                }
            } else {
                sendHelp(sender);
            }
        } else {
            sendHelp(sender);
        }
        return true;
    }

    public void sendHelp(CommandSender sender) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(sysprefix + "Hilfsübersicht:\n");
        stringBuilder.append(sysprefix + "§6/api reload (Pluginname)§7 Reloaded den Server bzw. ein Plugin!");
        sender.sendMessage(stringBuilder.toString());
    }
}
