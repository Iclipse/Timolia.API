package de.Iclipse.NRaid.API.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static de.Iclipse.NRaid.API.Main.MainData.prefix;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class cmd_colour implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefix + "Farbcodes:\n");
        stringBuilder.append("§00 - §0Schwarz§r\n");
        stringBuilder.append("§71 - §1Blau§r\n");
        stringBuilder.append("§72 - §2Grün§r\n");
        stringBuilder.append("§73 - §3Dunkeltürkis§r\n");
        stringBuilder.append("§74 - §4Rot§r\n");
        stringBuilder.append("§75 - §5Lila§r\n");
        stringBuilder.append("§76 - §6Orange§r\n");
        stringBuilder.append( "§77 - Grau§r\n");
        stringBuilder.append("§78 - §8Grau\n");
        stringBuilder.append("§79 - §9Dunkelblau§r\n");
        stringBuilder.append("§7a - §aHellgrün§r\n");
        stringBuilder.append("§7b - §bTürkis§r\n");
        stringBuilder.append("§7c - §cHellrot§r\n");
        stringBuilder.append("§7d - §dPink§r\n");
        stringBuilder.append("§7e - §eGelb§r\n");
        stringBuilder.append("§7r - §rWeiß§r\n");
        stringBuilder.append("§7l - §lFett§r\n");
        stringBuilder.append("§r§7n - §nUnterstrichen§r\n");
        stringBuilder.append("§r§7o - §oKurisv§r");
        sender.sendMessage(stringBuilder.toString());

        return false;
    }

}
