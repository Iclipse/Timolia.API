package de.Iclipse.NRaid.API.Commands.Completer;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Mode.getModes;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Mode.getSlot;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.getServers;
import static de.Iclipse.NRaid.API.Main.MainData.sysprefix;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 20.10.2017 at 22:03 o´ clock
 */
public class com_mode implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            String s = "create,delete,remove,add,removeALL,set,list,info,setLoc,hasLoc,setItem,getItem,hasItem,setName,setDisplayname,getDisplayname,getSlot,setSlot";
            String[] ss = s.split(",");
            return TComplete(ss, args, 0);
        } else {
            if (args[0].equalsIgnoreCase("create")) {
                if (args.length >= 3) {
                    return TComplete(getServers(), args, args.length - 1);
                }
            } else if (args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("removeall") || args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("hasLoc") || args[0].equalsIgnoreCase("getItem") || args[0].equalsIgnoreCase("hasItem") || args[0].equalsIgnoreCase("getDisplayname") || args[0].equalsIgnoreCase("setItem") || args[0].equalsIgnoreCase("setDisplayname") || args[0].equalsIgnoreCase("getSlot")) {
                if (args.length == 2) {
                    return TComplete(getModes(), args, 1);
                } else {
                    return null;
                }
            } else if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("set")) {
                if (args.length == 2) {
                    return TComplete(getModes(), args, args.length - 1);
                } else {
                    return TComplete(getServers(), args, args.length - 1);
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                return new ArrayList<>();
            } else if (args[0].equalsIgnoreCase("setLoc")) {
                if (args.length == 2) {
                    return TComplete(getModes(), args, args.length - 1);
                } else {
                    if (args.length == 3) {
                        if (sender instanceof Player) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(String.valueOf(((Player) sender).getLocation().getX()));
                            return list;
                        } else {
                            return null;
                        }
                    } else if (args.length == 4) {
                        if (sender instanceof Player) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(String.valueOf(((Player) sender).getLocation().getY()));
                            return list;
                        } else {
                            return null;
                        }
                    } else if (args.length == 5) {
                        if (sender instanceof Player) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(String.valueOf(((Player) sender).getLocation().getZ()));
                            return list;
                        } else {
                            return null;
                        }
                    } else if (args.length == 6) {
                        if (sender instanceof Player) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(String.valueOf(((Player) sender).getLocation().getYaw()));
                            return list;
                        } else {
                            return null;
                        }
                    } else if (args.length == 7) {
                        if (sender instanceof Player) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(String.valueOf(((Player) sender).getLocation().getPitch()));
                            return list;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            } else if (args[0].equalsIgnoreCase("setSlot")) {
                if (args.length == 2) {
                    return TComplete(getModes(), args, args.length - 1);
                } else if (args.length == 3) {
                    ArrayList<String> list = new ArrayList<>();
                    for (int i = 0; i < 36; i++) {
                        list.add(String.valueOf(i));
                    }
                    getModes().forEach(entry -> {
                        list.remove(getSlot(entry));
                    });
                    return TComplete(list, args, args.length - 1);
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
        return new ArrayList<>();
    }

    public static List<String> TComplete(String[] ss, String[] args, int z) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < ss.length; i++) {
            if (ss[i].startsWith(args[z])) {
                list.add(ss[i]);
            }
        }
        return list;
    }

    public static List<String> TComplete(ArrayList<String> l, String[] args, int z) {
        String[] ss = l.toArray(new String[l.size()]);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < ss.length; i++) {
            if (ss[i].startsWith(args[z])) {
                list.add(ss[i]);
            }
        }
        return list;
    }


    public static void sendMHelp(CommandSender s, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        if (i <= 2 || i >= 4) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 1)§7:");
            stringBuilder.append("\n" + sysprefix + "§c/mode create <Name> {Serverliste}§7 Erstellt einen Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode delete <ModeName> §7Löscht einen Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode add <ModeName> [Serverliste] §7Fügt zu einem Modus beliebig viele Server hinzu.");
            stringBuilder.append("\n" + sysprefix + "§c/mode remove <ModeName> [Serverliste] §7Entfernt die Server von dem Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode set <ModeName> [Serverliste] §7Legt die MySQL_Server für einen Modus fest.");
            stringBuilder.append("\n" + sysprefix + "§c/mode removeAll <ModeName> §7Löscht alle MySQL_Server von einem Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode list §7Gibt dir alle Modi aus.");
            s.sendMessage(stringBuilder.toString());
        } else if (i == 2) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 2)§7:");
            stringBuilder.append("\n" + sysprefix + "§c/mode info <ModeName> §7Gibt dir die Info eines Modus aus!");
            stringBuilder.append("\n" + sysprefix + "§c/mode setLoc <ModeName> {Location}§7Setzt die Location für den Modus in der Lobby.");
            stringBuilder.append("\n" + sysprefix + "§c/mode hasLoc <ModeName> §7Gibt dir zurück ob der Modus eine Location hat.");
            stringBuilder.append("\n" + sysprefix + "§c/mode setItem <ModeName> (Id) §7Das Item in deiner Hand wird als Modusitem gesetzt.");
            stringBuilder.append("\n" + sysprefix + "§c/mode getItem <ModeName> §7Das Modusitem wird dir in die Hand gelegt.");
            stringBuilder.append("\n" + sysprefix + "§c/mode hasItem <ModeName> §7Gibt dir zurück ob der Modus ein Item hat.");
        } else if (i == 3) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 3)§7:");
            stringBuilder.append("\n" + sysprefix + "§c/mode setName <ModeName> <NewModeName>§7Ändert den Namen eines Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode setDisplayName <ModeName> <DisplayName> §7Setzt den Displaynamen eines Modus.");
            stringBuilder.append("\n" + sysprefix + "§c/mode getDisplayName <ModeName> §7Gibt den Displaynamen eines Modus zurück.");
        }
    }
}
