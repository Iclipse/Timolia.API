package de.Iclipse.NRaid.API.Commands.Completer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.List;

import static de.Iclipse.NRaid.API.Commands.Completer.com_mode.TComplete;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.getServers;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 07.11.2017 at 19:09 o´ clock
 */
public class com_server implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            String s = "status,maxplayer,";
            String[] ss = s.split(",");
            return TComplete(ss, args, 0);
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("status") || args[0].equalsIgnoreCase("maxplayer")) {
                return TComplete(getServers(), args, 1);
            }
        } else if (args[0].equalsIgnoreCase("status") && args.length == 3) {
            String s = "Lobby,Ingame,Online,Wartung";
            String[] ss = s.split(",");
            return TComplete(ss, args, 0);
        }
        return null;
    }
}
