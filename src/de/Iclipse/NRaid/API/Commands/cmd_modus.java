package de.Iclipse.NRaid.API.Commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Mode.*;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.isServerRegistered;
import static de.Iclipse.NRaid.API.Main.Main.sendJSONMassage;
import static de.Iclipse.NRaid.API.Main.MainData.sysprefix;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 05.11.2017 at 12:46 o´ clock
 */
public class cmd_modus implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sendMHelp(sender, 0);
        } else {
            if (args[0].equalsIgnoreCase("create")) {
                if (args.length == 1) {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode create <Modename> {Serverliste}");
                } else {
                    if (args.length == 2) {
                        if (!isModeExists(args[1])) {
                            registerMode(args[1]);
                            if (isModeExists(args[1])) {
                                sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 wurde erfolgreich erstellt!");
                            } else {
                                sender.sendMessage(sysprefix + "§cBeim Erstellen des Modus§5 " + args[1] + "§c ist ein Fehler aufgetreten!");
                            }
                        } else {
                            sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 existiert bereits!");
                        }
                    } else {
                        if (!isModeExists(args[1])) {
                            registerMode(args[1]);
                            if (isModeExists(args[1])) {
                                sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 wurde erfolgreich erstellt!");
                            } else {
                                sender.sendMessage(sysprefix + "§cBeim Erstellen des Modus§5 " + args[1] + "§c ist ein Fehler aufgetreten!");
                            }
                        } else {
                            if (sender instanceof Player) {
                                sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 existiert bereits, sollen die Server... werden?");
                                String s = "";
                                for (int i = 2; i < args.length; i++) {
                                    s = s + args[i] + " ";
                                }
                                sendJSONMassage((Player) sender, "{\"text\":\"§2...Überschrieben      \",\"hoverEvent\":{\"action\":\"show_text\", \"value\":\"..Überschrieben werden \"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/mode set " + args[1] + " " + s + " \"}, \"extra\":[{\"text\":\"...Hinzugefügt\",\"hoverEvent\":{\"action\":\"show_text\", \"value\":\"...Hinzugefügt werden\"}, \"clickEvent\":{\"action\":\"run_command\", \"value\":\"/mode add " + args[1] + " " + s + "\"}}]}");

                            }
                        }
                    }
                }
            } else if (args[0].equalsIgnoreCase("delete")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        deleteMode(args[1]);
                        if (isModeExists(args[1])) {
                            sender.sendMessage(sysprefix + "§cEs ist ein Fehler aufgetreten, der Modus konnte nicht gelöscht werden!");
                        } else {
                            sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 wurde erfolgreich gelöscht!");
                        }
                    } else {
                        sender.sendMessage(sysprefix + "§cDieser Modus existiert nicht!");
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode delete <Modename>");
                }
            } else if (args[0].equalsIgnoreCase("add")) {
                if (args.length == 2) {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode add <ModeName> [Serverliste]");
                } else {
                    if (isModeExists(args[1])) {
                        String reg = "";
                        String nreg = "";
                        String areg = "";
                        for (int i = 2; i < args.length; i++) {
                            if (isServerRegistered(args[i])) {
                                if (!hasServer(args[1], args[i])) {
                                    addServer(args[1], args[i]);
                                    if (reg == "") {
                                        reg = reg + args[i];
                                    } else {
                                        reg = reg + ", " + args[i];
                                    }
                                } else {
                                    if (areg == "") {
                                        areg = areg + args[i];
                                    } else {
                                        areg = areg + ", " + args[i];
                                    }
                                }
                            } else {
                                if (nreg == "") {
                                    nreg = nreg + args[i];
                                } else {
                                    nreg = nreg + ", " + args[i];
                                }
                            }
                        }
                        if (reg != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + reg + "§7 wurden erfolgreich dem Modus hinzugefügt!");
                        }
                        if (areg != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + areg + "§7sind bereits in diesem Modus vorhan");
                        }
                        if (nreg != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + nreg + "§7 sind nicht registriert!");
                        }
                    } else {
                        sender.sendMessage(sysprefix + "Dieser Modus existiert nicht, soll er erstellt werden?");
                        String s = "";
                        for (int i = 2; i < args.length; i++) {
                            s = s + args[i] + " ";
                        }
                        NotExistsCreate(sender, args, 2);
                    }
                }
            } else if (args[0].equalsIgnoreCase("remove")) {
                if (args.length <= 2) {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode remove <ModeName> [Serverliste]");
                } else {
                    if (isModeExists(args[1])) {
                        String rem = "";
                        String nreg = "";
                        String nrem = "";
                        for (int i = 2; i < args.length; i++) {
                            if (isServerRegistered(args[i])) {
                                if (hasServer(args[1], args[i])) {
                                    addServer(args[1], args[i]);
                                    if (rem == "") {
                                        rem = rem + args[i];
                                    } else {
                                        rem = rem + ", " + args[i];
                                    }
                                } else {
                                    if (nrem == "") {
                                        nrem = nrem + args[i];
                                    } else {
                                        nrem = nrem + ", " + args[i];
                                    }
                                }
                            } else {
                                if (nreg == "") {
                                    nreg = nreg + args[i];
                                } else {
                                    nreg = nreg + ", " + args[i];
                                }
                            }
                        }
                        if (rem != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + rem + "§7 wurden erfolgreich entfernt!");
                        }
                        if (nrem != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + nrem + "§7 sind nicht in diesem Modus vorhanden");
                        }
                        if (nreg != "") {
                            sender.sendMessage(sysprefix + "Die Server §5" + nreg + "§7 sind nicht registriert!");
                        }
                    } else {
                        sender.sendMessage(sysprefix + "Dieser Modus existiert nicht!");
                    }
                }
            } else if (args[0].equalsIgnoreCase("set")) {
                if (args.length <= 2) {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode set <ModeName> [Serverliste]");
                    return true;
                }
                if (isModeExists(args[1])) {
                    ArrayList<String> list = new ArrayList<>();
                    ArrayList<String> list2 = getServers(args[1]);
                    String nreg = "";
                    String rem = "";
                    String add = "";
                    for (int i = 2; i < args.length; i++) {
                        if (isServerRegistered(args[i])) {
                            list.add(args[i]);
                        } else {
                            String se = "";
                            for (int s = i + 1; s < args.length; s++) {
                                se = args[i] + " " + args[s];
                                if (isServerRegistered(se)) {
                                    list.add(args[i]);
                                    i = i + (s - i);
                                }
                            }
                        }
                        if (nreg == "") {
                            nreg = nreg + args[i];
                        } else {
                            nreg = nreg + ", " + args[i];
                        }
                    }
                    for (int i = 0; i < list2.size(); i++) {
                        if (!list.contains(list2.get(i))) {
                            if (rem == "") {
                                rem = rem + list2.get(i);
                            } else {
                                rem = rem + ", " + list2.get(i);
                            }
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (!list2.contains(list.get(i))) {
                            if (add == "") {
                                add = add + list.get(i);
                            } else {
                                add = add + ", " + list.get(i);
                            }
                        }
                    }
                    if (add != "") {
                        sender.sendMessage(sysprefix + "Die Server §5" + add + "§7 wurden erfolgreich hinzugefügt!");
                    }
                    if (rem != "") {
                        sender.sendMessage(sysprefix + "Die Server §5" + rem + "§7 wurden erfolgreich entfernt!");
                    }
                    if (nreg != "") {
                        sender.sendMessage(sysprefix + "Die Server §5" + nreg + "§7 sind nicht registriert!");
                    }
                } else {
                    NotExistsCreate(sender, args, 2);
                }
            } else if (args[0].equalsIgnoreCase("removeall")) {
                if (args.length <= 2) {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode removeAll <ModeName>");
                    return true;
                }
                if (isModeExists(args[1])) {
                    removeAllServers(args[1]);
                    if (getServers(args[1]).size() != 0) {
                        sender.sendMessage(sysprefix + "§cBeim Entfernen aller Server ist ein Fehler aufgetreten!");
                    } else {
                        sender.sendMessage(sysprefix + "Die Server des Modus §5" + args[1] + "§7 wurden erfolgreich entfernt!");
                    }
                } else {
                    sender.sendMessage(sysprefix + "Dieser Modus existiert nicht, soll er erstellt werden?");
                    NotExistsCreate(sender, args[1]);
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                if (args.length == 1) {
                    sender.sendMessage(sysprefix + "Es existieren folgende Modi:");
                    final String[] message = {""};
                    getModes().forEach(entry -> {
                        System.out.println(sysprefix + entry);
                        message[0] = message[0] + "§5" + entry + "§7, ";
                        System.out.println(message[0]);
                    });
                    sender.sendMessage(sysprefix + message[0]);
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode list");
                }
            } else if (args[0].equalsIgnoreCase("info")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        sender.sendMessage(getModeInfo(args[1]));
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode info <ModeName>");
                }
            } else if (args[0].equalsIgnoreCase("setLoc")) {
                if (args.length == 5 || args.length == 2 || args.length == 7) {
                    if (isModeExists(args[1])) {
                        if (args.length == 2) {
                            if (sender instanceof Player) {
                                setLocation(args[1], (Player) sender);
                                sender.sendMessage(sysprefix + "Die Location für den Modus §5" + args[1] + "§7 wurde erfolgreich gesetzt!");
                            } else {
                                sender.sendMessage(sysprefix + "§cBenutzung: /mode setLoc <ModeName> [Location]");
                            }
                        } else {
                            try {
                                setLocation(args[1], new Location(Bukkit.getWorld("world"), Double.parseDouble(args[2]), Double.parseDouble(args[3]), Double.parseDouble(args[4]), Float.parseFloat(args[5]), Float.parseFloat(args[6])));
                            } catch (ArrayIndexOutOfBoundsException e) {
                                setLocation(args[1], new Location(Bukkit.getWorld("world"), Double.parseDouble(args[2]), Double.parseDouble(args[3]), Double.parseDouble(args[4])));
                            } catch (NumberFormatException e) {
                                sender.sendMessage(sysprefix + "§cBenutzung: /mode setLoc <ModeName> [Location]");
                            }
                            sender.sendMessage(sysprefix + "Die Location für den Modus §5" + args[1] + "§7 wurde erfolgreich gesetzt!");
                        }
                    } else {
                        NotExistsCreate(sender, args[0]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode setLoc <ModeName> {Location}");
                }
            } else if (args[0].equalsIgnoreCase("hasLoc")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        if (hasLocation(args[1])) {
                            Location loc = getLocation(args[1]);
                            sender.sendMessage(sysprefix + "§7Der Modus §5" + args[1] + "§7 hat eine Location welche bei §6" + loc.getBlockX() + "§7, §6" + loc.getBlockY() + "§7, §6" + loc.getBlockZ() + "liegt!");
                        } else {
                            sender.sendMessage(sysprefix + "Der Modus hat keine Location!");
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage("§6Benutzung: /mode hasLoc <ModeName>");
                }
            } else if (args[0].equalsIgnoreCase("setItem")) {
                if (args.length == 2 || args.length == 3) {
                    if (isModeExists(args[1])) {
                        if (args.length == 3) {
                            try {
                                int id = Integer.parseInt(args[2]);
                                if (id <= 0 || id > 197) {
                                    sender.sendMessage(sysprefix + "§cDieses Item existiert nicht!");
                                } else {
                                    setItem(args[1], id);
                                    sender.sendMessage(sysprefix + "Das Item wurde erfolgreich gesetzt!");
                                }
                            } catch (Exception e) {
                                sender.sendMessage(sysprefix + "§cBenutzung: /mode setItem <ModeName> (Id)");
                            }
                        } else {
                            if (sender instanceof Player) {
                                try {
                                    if (((Player) sender).getItemInHand().getTypeId() != 0) {
                                        setItem(args[1], ((Player) sender).getItemInHand().getTypeId());
                                        sender.sendMessage(sysprefix + "Das Item wurde erfolgreich gesetzt!");
                                    } else {
                                        sender.sendMessage(sysprefix + "§cDu hast kein Item in der Hand!");
                                    }
                                } catch (Exception e) {
                                    sender.sendMessage(sysprefix + "Du hast kein Item in der Hand!");
                                    sender.sendMessage(sysprefix + "Das Item wurde erfolgreich gesetzt!");
                                }
                            } else {
                                sender.sendMessage(sysprefix + "§6Benutzung: /mode setItem <ModeName> [Id]");
                            }
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§6Benutzung: /mode setItem <ModeName> (Id)");
                }
            } else if (args[0].equalsIgnoreCase("getItem")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        if (hasItem(args[1])) {
                            if (sender instanceof Player) {
                                if (((Player) sender).getItemInHand().getTypeId() == 0) {
                                    ((Player) sender).setItemInHand(getItem(args[1]));
                                } else {
                                    for (int i = 0; i < 9; i++) {
                                        if (((Player) sender).getInventory().getItem(i).getTypeId() == 0) {
                                            ((Player) sender).getInventory().setItem(i, getItem(args[1]));
                                            sender.sendMessage(sysprefix + "Du hast das Item jetzt im Inventar!");
                                            return true;
                                        }
                                    }
                                    for (int i = 35; i > 8; i++) {
                                        if (((Player) sender).getInventory().getItem(i).getTypeId() == 0) {
                                            ((Player) sender).getInventory().setItem(i, getItem(args[1]));
                                            sender.sendMessage(sysprefix + "Du hast das Item jetzt im Inventar!");
                                            return true;
                                        }
                                    }
                                    sender.sendMessage(sysprefix + "Du hast kein Platz in deinem Inventar!");
                                }
                            } else {
                                sender.sendMessage(sysprefix + "Der Block hat die ID §6" + getItemId(args[1]));
                            }
                        } else {
                            sender.sendMessage(sysprefix + "§cDer Modus hat noch kein Item!");
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode getItem <ModeName>");
                }
            } else if (args[0].equalsIgnoreCase("hasItem")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        if (hasItem(args[1])) {
                            sender.sendMessage(sysprefix + "Der Modus hat ein Item!");
                        } else {
                            sender.sendMessage(sysprefix + "Der Modus hat kein Item!");
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§6Benutzung: /mode hasItem <ModeName>");
                }
            } else if (args[0].equalsIgnoreCase("setSlot")) {
                if (args.length == 3) {
                    if (isModeExists(args[1])) {
                        try {
                            int slot = Integer.parseInt(args[2]);
                            if (getSlot(args[1]) != slot) {
                                setSlot(args[1], slot);
                                sender.sendMessage(sysprefix + "Der Slot von §5" + args[1] + "§7 wurde erfolgreich zu §6" + slot + "§7 geändert!");
                            } else {
                                sender.sendMessage(sysprefix + "Der Slot von §5" + args[1] + "§7 war bereits §6" + slot + "§7!");
                            }
                        } catch (NumberFormatException e) {
                            sender.sendMessage(sysprefix + "§cBenutzung: /mode setSlot <Modename> <Slot>");
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode setSlot <Modename> <Slot>");
                }
            } else if (args[0].equalsIgnoreCase("getSlot")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        if (hasSlot(args[1])) {
                            sender.sendMessage(sysprefix + "Der Slot von §5" + args[1] + " ist bei §6" + getSlot(args[1]) + "§7!");
                        } else {
                            sender.sendMessage(sysprefix + "Der Modus §5" + args[1] + " hat §ckeinen§7 Slot!");
                        }
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode getSlot <Modename>");
                }
            } else if (args[0].equalsIgnoreCase("setName")) {
                if (args.length == 3) {
                    if (isModeExists(args[1])) {
                        if (isModeExists(args[2])) {
                            setModeName(args[1], args[2]);
                        } else {
                            sender.sendMessage(sysprefix + "§cDer Modus §5" + args[2] + " existiert bereits!");
                        }
                    } else {
                        if (sender instanceof Player) {
                            Player p = (Player) sender;
                            p.sendMessage(sysprefix + "Der Modus §5" + args[1] + "§7 existiert nicht, soll der Modus mit dem neuen Namen §5" + args[2] + "§7 erstellt werden?");
                            NotExistsCreate(sender, args[1]);
                        } else {
                            sender.sendMessage(sysprefix + "Dieser Modus existiert nicht!");
                        }
                    }
                } else {
                    sender.sendMessage("§6Benutzung: /mode setName <ModeName> <NewModeName>");
                }
            } else if (args[0].equalsIgnoreCase("setDisplayName")) {
                if (args.length == 3) {
                    if (isModeExists(args[1])) {
                        setDisplayname(args[1], args[2]);
                    } else {
                        NotExistsCreate(sender, args[1]);
                        sender.sendMessage(sysprefix + "Du musst den Befehl danach erneut ausführen!");
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode setDisplayName");
                }
            } else if (args[0].equalsIgnoreCase("getDisplayname")) {
                if (args.length == 2) {
                    if (isModeExists(args[1])) {
                        sender.sendMessage(sysprefix + "Displayname von §5" + args[1] + "§7: " + getDisplayname(args[1]));
                    } else {
                        NotExistsCreate(sender, args[1]);
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode getDisplayname <ModeName>");
                }
            }
        }
        return true;
    }

    public static void sendMHelp(CommandSender s, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        if (i <= 2 || i >= 4) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 1)§7:");
            stringBuilder.append("\n" + sysprefix + "§6/mode create <Name> {Serverliste}§7 Erstellt einen Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode delete <ModeName> §7Löscht einen Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode add <ModeName> [Serverliste] §7Fügt zu einem Modus beliebig viele Server hinzu.");
            stringBuilder.append("\n" + sysprefix + "§6/mode remove <ModeName> [Serverliste] §7Entfernt die Server von dem Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode set <ModeName> [Serverliste] §7Legt die Server für einen Modus fest.");
            stringBuilder.append("\n" + sysprefix + "§6/mode removeAll <ModeName> §7Löscht alle Server von einem Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode list §7Gibt dir alle Modi aus.");
            s.sendMessage(stringBuilder.toString());
        } else if (i == 2) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 2)§7:");
            stringBuilder.append("\n" + sysprefix + "§6/mode info <ModeName> §7Gibt dir die Info eines Modus aus!");
            stringBuilder.append("\n" + sysprefix + "§6/mode setLoc <ModeName> {Location}§7Setzt die Location für den Modus in der Lobby.");
            stringBuilder.append("\n" + sysprefix + "§6/mode hasLoc <ModeName> §7Gibt dir zurück ob der Modus eine Location hat.");
            stringBuilder.append("\n" + sysprefix + "§6/mode setItem <ModeName> (Id) §7Das Item in deiner Hand wird als Modusitem gesetzt.");
            stringBuilder.append("\n" + sysprefix + "§6/mode getItem <ModeName> §7Das Modusitem wird dir in die Hand gelegt.");
            stringBuilder.append("\n" + sysprefix + "§6/mode hasItem <ModeName> §7Gibt dir zurück ob der Modus ein Item hat.");
        } else if (i == 3) {
            stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 3)§7:");
            stringBuilder.append("\n" + sysprefix + "§6/mode setName <ModeName> §7Ändert den Namen eines Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode setDisplayName <ModeName> <DisplayName> §7Setzt den Displaynamen eines Modus.");
            stringBuilder.append("\n" + sysprefix + "§6/mode getDisplayName <ModeName> §7Gibt den Displaynamen eines Modus zurück.");
        }
    }

    public static void NotExistsCreate(CommandSender sender, String name) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage(sysprefix + "Dieser Modus existiert nicht, soll er erstellt werden?");
            /*
            TextComponent tc = new TextComponent("Ja\t");
            tc.setColor(ChatColor.GREEN);
            tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mode create " + name));
            tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Ja").create()));
            TextComponent tc2 = new TextComponent("Nein");
            tc2.setColor(ChatColor.RED);
            tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Nein").create()));
            tc.addExtra(tc2);
            ((Player) sender).spigot().sendMessage(tc);
            */
            sendJSONMassage(p, "{\"text\":\"§2Ja \",\"hoverEvent\":{\"action\":\"show_text\", \"value\":\"Ja \"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/mode create " + name + " \"}}");
        } else {
            sender.sendMessage(sysprefix + "Dieser Modus existiert nicht!");
        }
    }


    public static void NotExistsCreate(CommandSender sender, String[] args, int i) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage(sysprefix + "Dieser Modus existiert nicht, soll er erstellt werden?");
            String s = "";
            for (int z = i; z < args.length; z++) {
                s = s + args[i] + " ";
            }
            sendJSONMassage(p, "{\"text\":\"§2Ja \",\"hoverEvent\":{\"action\":\"show_text\", \"value\":\"Ja \"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/mode create " + args[1] + " " + s + " \"}}");
        } else {
            sender.sendMessage(sysprefix + "Dieser Modus existiert nicht!");
        }
    }

    public static String getModeInfo(String modename) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(sysprefix + "§5" + modename + "§7:\n");
        final String[] servers = {""};
        try {
            getServers(modename).forEach(entry -> {
                if (!entry.equals("None")) {
                    servers[0] = servers[0] + "§5" + entry + "§7, ";
                } else {
                    servers[0] = "§cKein Server";
                }
            });
        } catch (NullPointerException e) {
            servers[0] = "§cKein Server";
        }
        stringBuilder.append(sysprefix + "§8Servers: " + servers[0] + "\n");
        Location loc = getLocation(modename);
        if (hasLocation(modename)) {
            stringBuilder.append(sysprefix + "§8Location: §6" + loc.getBlockX() + "§7, §6" + loc.getBlockY() + "§7, §6" + loc.getBlockZ() + "\n");
        } else {
            stringBuilder.append(sysprefix + "§8Location: §cKeine Location!\n");
        }
        stringBuilder.append(sysprefix + "§8Displayname: §7" + getDisplayname(modename) + "\n");
        if (hasItem(modename)) {
            stringBuilder.append(sysprefix + "§8Item: §7" + getItem(modename).toString() + "\n");
        } else {
            stringBuilder.append(sysprefix + "§8Item: §cKein Item!\n");
        }
        if (hasSlot(modename)) {
            stringBuilder.append(sysprefix + "§8Slot: §7" + getSlot(modename) + "\n");
        } else {
            stringBuilder.append(sysprefix + "§8Slot: §cKein Slot!\n");
        }
        return stringBuilder.toString();
    }
}
