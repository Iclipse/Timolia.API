package de.Iclipse.NRaid.API.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.*;
import static de.Iclipse.NRaid.API.Main.MainData.sysprefix;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 07.11.2017 at 18:37 o´ clock
 */
public class cmd_server implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sendSHelp(sender);
        } else {
            if (args[0].equalsIgnoreCase("status")) {
                if (args.length == 2) {
                    changeServerStatus(Bukkit.getServerName(), args[1]);
                    sender.sendMessage(sysprefix + "Der Status des Servers §5" + Bukkit.getServerName() + "§7 wurde erfolgreich zu §5" + args[1] + "§7 geändert!");
                } else if (args.length == 3) {
                    if (isServerRegistered(args[1])) {
                        changeServerStatus(args[1], args[2]);
                        sender.sendMessage(sysprefix + "Der Status des Servers §5" + args[1] + "§7 wurde erfolgreich zu §5" + args[2] + "§7 geändert!");
                    /*
                    }else if(isServerRegistered(args[2])){
                        changeServerStatus(Bukkit.getServerName(), args[1]);
                        sender.sendMessage(sysprefix + "Der Status des Servers §5" + args[2] + "§7 wurde erfolgreich zu §5" + args[1]  + "§7 geändert!");
                    }else{
                    */
                        sender.sendMessage(sysprefix + "Der Server existiert nicht!");
                    }
                } else {
                    sender.sendMessage(sysprefix + "§cBenutzung: /mode status (Name) <Status>");
                }
            } else if (args[0].equalsIgnoreCase("maxplayer")) {
                if (args.length == 3) {
                    if (isServerRegistered(args[1])) {
                        try {
                            setMaxPlayers(args[1], Integer.parseInt(args[2]));
                        } catch (NumberFormatException e) {
                            sender.sendMessage(sysprefix + "Du musst eine Zahl angeben!");
                        }
                    } else {
                        sender.sendMessage(sysprefix + "Dieser Server existiert nicht!");
                    }
                } else {
                    sender.sendMessage(sysprefix + "§6Benutzung: /servers maxplayer <ServerName> <MaxPlayers>");
                }
            }
        }
        return false;
    }

    public static void sendSHelp(CommandSender sender) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n" + sysprefix + "Hilfsübersicht §8(Seite 1)§7:");
        stringBuilder.append("\n" + sysprefix + "§c/servers status (Name) <Status>§7 Ändert einen Serverstatus.");
        stringBuilder.append("\n" + sysprefix + "§c/servers maxplayer (Name) <Anzahl> §7Ändert die maximale Spieleranzahl");
        sender.sendMessage(stringBuilder.toString());
    }

}
