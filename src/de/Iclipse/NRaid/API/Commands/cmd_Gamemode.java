package de.Iclipse.NRaid.API.Commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static de.Iclipse.NRaid.API.Main.MainData.gamememodeprefix;
import static de.Iclipse.NRaid.API.Main.MainData.noperm;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class cmd_Gamemode implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender.hasPermission("nraid.builder.gamemode")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (args.length == 0) {
                    //    if(!isVanish(p)) {
                    setModeWOArgs(p, "!");
                    //    }else{
                    //       setModeWOArgs(p, ", bleibst aber unsichtbar!");
                    //   }
                } else {
                    if (args.length == 1) {
                        //    if(!isVanish(p)) {
                        setModeW1Arg(p, args[0], "!");
                        //    }else{
                        //        setModeW1Arg(p, args[0], ", bleibst aber unsichtbar!");
                        //    }
                    } else {
                        if (args.length == 2) {
                            Bukkit.getOnlinePlayers().forEach(pp -> {
                                if (args[0].equalsIgnoreCase(pp.getName())) {
                                    //      if(!isVanish(pp)) {
                                    setModeW1Arg(pp, args[1], "!");
                                    //      }else{
                                    //          setModeW1Arg(pp, args[1], ", bleibst aber unsichtbar!");
                                    //      }
                                } else {
                                    if (args[1].equalsIgnoreCase(pp.getName())) {
                                        //          if(!isVanish(pp)) {
                                        setModeW1Arg(pp, args[0], "!");
                                        //          }else{
                                        //              setModeW1Arg(pp, args[0], ", bleibst aber unsichtbar!");
                                        //          }
                                    }
                                }
                            });
                        }
                    }
                }
                return true;
            }
        } else {
            sender.sendMessage(noperm);
        }
        return true;
    }

    public static void setModeWOArgs(Player p, String s){
        if(p.getGameMode() == GameMode.CREATIVE){
            p.setGameMode(GameMode.SURVIVAL);
            p.sendMessage(gamememodeprefix + "Du bist nun im Survival Mode" + s);
        }else{
            if(p.getGameMode() == GameMode.SPECTATOR){
                p.setGameMode(GameMode.SURVIVAL);
                p.sendMessage(gamememodeprefix + "Du bist nun im Survival Mode" + s);
            }else{
                p.setGameMode(GameMode.CREATIVE);
                p.sendMessage(gamememodeprefix + "Du bist nun im Creative Mode" + s);
            }
        }
    }
    public static void setModeW1Arg(Player p, String arg, String s){
        if (arg.equalsIgnoreCase("Survival") || arg.equalsIgnoreCase("ueberleben") || arg.equalsIgnoreCase("überleben") || arg.equalsIgnoreCase("0")) {
            p.setGameMode(GameMode.SURVIVAL);
            p.sendMessage(gamememodeprefix + "Du bist nun im Survival Mode" + s);
        } else {
            if (arg.equalsIgnoreCase("Creative") || arg.equalsIgnoreCase("Kreativ") || arg.equalsIgnoreCase("1")) {
                p.setGameMode(GameMode.CREATIVE);
                p.sendMessage(gamememodeprefix + "Du bist nun im Creative Mode" + s);
            } else {
                if (arg.equalsIgnoreCase("Spectator") || arg.equalsIgnoreCase("Spec") || arg.equalsIgnoreCase("Zuschauer") || arg.equalsIgnoreCase("3")) {
                    p.setGameMode(GameMode.SPECTATOR);
                    p.sendMessage(gamememodeprefix + "Du bist nun im Spectator Mode" + s);
                } else {
                    if (arg.equalsIgnoreCase("Adventure") || arg.equalsIgnoreCase("Abenteuer") || arg.equalsIgnoreCase("2")) {
                        p.setGameMode(GameMode.ADVENTURE);
                        p.sendMessage(gamememodeprefix + "Du bist nun im Adventure Mode" + s);
                    } else {
                        Bukkit.getOnlinePlayers().forEach(pp -> {
                            if (arg.equalsIgnoreCase(pp.getName())) {
                      //          if(!isVanish(pp)) {
                                    setModeWOArgs(pp, "!");
                      //          }else{
                      //              setModeWOArgs(pp, ", bleibst aber unsichtbar!");
                      //          }
                            } else {
                                p.sendMessage(gamememodeprefix + "Du musst einen vorhandenen Gamemode oder einen Spielernamen angeben!");
                            }
                        });

                    }
                }
            }
        }
    }

    public static ArrayList<String> getAliases(){
        ArrayList<String> aliases = new ArrayList<>();
        aliases.add("gamemode");
        aliases.add("gm");
        aliases.add("Gmode");
        return aliases;
    }
}
