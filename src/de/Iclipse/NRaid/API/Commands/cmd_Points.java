package de.Iclipse.NRaid.API.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_User.*;
import static de.Iclipse.NRaid.API.Functions.UUIDFetcher.getUUID;
import static de.Iclipse.NRaid.API.Main.MainData.statisticsprefix;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class cmd_Points implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("points")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    p.sendMessage(statisticsprefix + "Du hast zurzeit §6§l" + getPoints(getUUID(p.getName())) + "§7 Punkte!");
                } else {
                    return false;
                }
                return true;
            } else {
                UUID uuid = getUUID(args[0]);
                sender.sendMessage("Der Spieler §e" + args[0] + "§7 hat §e" + getPoints(uuid) + "§7 Punkte! ");
                return true;
            }
        }else{
            if(cmd.getName().equalsIgnoreCase("setPoints")){
                if(args.length == 1){
                    if(sender instanceof Player) {
                        int z ;
                        try{
                            z = Integer.parseInt(args[0]);
                        }catch (Exception e){
                            sender.sendMessage(statisticsprefix + "§c/setPoints <Points>");
                            return true;
                        }
                        setPoints(getUUID(((Player) sender).getName()), z);
                        sender.sendMessage(statisticsprefix + "Du hast nun §e" + args[0] + "§7 Punkte!");
                    }else{
                        return false;
                    }
                    }else {
                    if (args.length == 2) {
                        int z;
                        UUID uuid = null;
                        try {
                            getUUID(args[1]);
                        } catch (Exception e) {
                            sender.sendMessage(statisticsprefix + "§c" + args[1] + " war noch nie online!");
                            return true;
                        }
                        if (!isUserExists(uuid)) {
                            try {
                                z = Integer.parseInt(args[1]);
                            } catch (Exception e) {
                                sender.sendMessage(statisticsprefix + "§c/setPoints <Name> <Points>");
                                return true;
                            }
                            setPoints(getUUID(((Player) sender).getName()), z);
                            sender.sendMessage(statisticsprefix + args[0] + " hat nun §e" + args[1] + "§7 Punkte!");
                        }else{
                            sender.sendMessage(statisticsprefix + "§c" + args[1] + " war noch nie online!");
                        }
                    }
                }
            }else{
                if(cmd.getName().equalsIgnoreCase("addPoints")){
                    if(args.length == 1){
                        if(sender instanceof Player) {
                            int z ;
                            try{
                                z = Integer.parseInt(args[0]);
                            }catch (Exception e){
                                sender.sendMessage(statisticsprefix + "§c/addPoints <Points>");
                                return true;
                            }
                            addPoints(getUUID(((Player) sender).getName()), z);
                            sender.sendMessage(statisticsprefix + "Du hast nun §e" +  getPoints(getUUID(((Player) sender).getName())) + "§7 Punkte!");
                        }else{
                            return false;
                        }
                    }else {
                        if (args.length == 2) {
                            int z;
                            UUID uuid = null;
                            try {
                                getUUID(args[1]);
                            } catch (Exception e) {
                                sender.sendMessage(statisticsprefix + "§c" + args[1] + " war noch nie online!");
                                return true;
                            }
                            if (!isUserExists(uuid)) {
                                try {
                                    z = Integer.parseInt(args[1]);
                                } catch (Exception e) {
                                    sender.sendMessage(statisticsprefix + "§c/addPoints <Name> <Points>");
                                    return true;
                                }
                                addPoints(getUUID(((Player) sender).getName()), z);
                                sender.sendMessage(statisticsprefix + args[0] + " hat nun §e" + getPoints(uuid) + "§7 Punkte!");
                            }else{
                                sender.sendMessage(statisticsprefix + "§c" + args[1] + " war noch nie online!");
                            }
                        }
                    }
                }else{
                    if(cmd.getName().equalsIgnoreCase("removePoints")){
                        if(args.length == 1){
                            if(sender instanceof Player) {
                                int z ;
                                try{
                                    z = Integer.parseInt(args[0]);
                                }catch (Exception e){
                                    sender.sendMessage(statisticsprefix + "§c/removePoints <Points>");
                                    return true;
                                }
                                removePoints(getUUID(((Player) sender).getName()), z);
                                sender.sendMessage(statisticsprefix + "Du hast nun §e" + getPoints(getUUID(((Player) sender).getName())) + "§7 Punkte!");
                            }else{
                                return false;
                            }
                        }else {
                            if (args.length == 2) {
                                int z;
                                UUID uuid = null;
                                try {
                                    getUUID(args[1]);
                                } catch (Exception e) {
                                    sender.sendMessage(statisticsprefix + "§e" + args[1] + " war noch nie online!");
                                    return true;
                                }
                                if (!isUserExists(uuid)) {
                                    try {
                                        z = Integer.parseInt(args[1]);
                                    } catch (Exception e) {
                                        sender.sendMessage(statisticsprefix + "§c/removePoints <Name> <Points>");
                                        return true;
                                    }
                                    removePoints(getUUID(((Player) sender).getName()), z);
                                    sender.sendMessage(statisticsprefix + args[0] + " hat nun §c" + getPoints(uuid) + "§7 Punkte!");
                                }else{
                                    sender.sendMessage(statisticsprefix + "§c" + args[1] + " war noch nie online!");
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
