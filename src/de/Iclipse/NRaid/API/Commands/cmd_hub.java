package de.Iclipse.NRaid.API.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Mode.getServers;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.getPlayers;
import static de.Iclipse.NRaid.API.Functions.MySQL.MySQL_Server.getStatus;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 03.12.2017 at 14:46 o´ clock
 */
public class cmd_hub implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 0) {
                String lobby;
                int planz = 100;
                ArrayList<String> servers = getServers("Lobby");
                for (int i = 0; i < servers.size(); i++) {
                    if (i == 0) {
                        lobby = servers.get(i);
                        planz = getPlayers(servers.get(i)).size();
                    } else {
                        if (getStatus(servers.get(i)).equalsIgnoreCase("Lobby")) {
                            int players = getPlayers(servers.get(i)).size();
                            if (players < planz) {
                                lobby = servers.get(i);
                                planz = players;
                            }
                        }
                    }
                }

            }
        } else {
            return false;
        }
        return true;
    }
}
